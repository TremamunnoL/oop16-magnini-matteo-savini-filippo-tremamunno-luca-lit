package oop.lit.model.simplegame.elements;

import oop.lit.model.elements.BoardElement;

/**
 * A board element for a simple game.
 */
public interface SimpleBoardElement extends BoardElement, SimpleElement {
}
