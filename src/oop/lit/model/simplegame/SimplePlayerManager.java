package oop.lit.model.simplegame;

import oop.lit.model.game.PlayerManagerImpl;

/**
 * A player manager for a simple game.
 */
public class SimplePlayerManager extends PlayerManagerImpl<SimplePlayer, SimplePlayerHand> {
    /**
     * 
     */
    private static final long serialVersionUID = -4634930691222328556L;
    //lo uso per abbreviare.
}
